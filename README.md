## Objective

The objective of this experiment is to represent a floating point number in binary system while following IEEE conventions.

## REQUIREMENTS(for linux based OS):

Make sure you have installed the following the recommended versions are also provided:

python3 (3.5.2)
(if pip3 is not installed use command sudo apt install python3-pip ) <br/>
Flask==1.0.2   (command to install pip3 install flask --user) <br/>
Flask_SQLAlchemy==2.2(command to install pip3 install flask_sqlalchemy --user) <br/> 
Install this plugin for chrome link : https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi?hl=en <br/>
Install this plugin for firefox link : https://addons.mozilla.org/en-US/firefox/addon/cors-everywhere/ <br/>
## Structure of the code

### File Description

-- run.py –> The main python file which runs the server <br/>
-- app.db –> This is the main database file which contains "Question" and "Answer" tables from which data will retrieved. <br/>
-- __init__.py –> This is where the flask application object named "app" is created and database configuration is done. <br/> 
    The project is modularized based on the MVC framework. The concept of Blueprints in Flask is used to register the different controllers in the project. <br/>
-- controllers/QuizController.py <br/> This Controller is responsible for routing Quiz related request calls to respective service. <br/>
-- service/QuizService.py <br/> This is the service class that actually processes the request made by the client. It is responsible to make to make a call to the QuizData methods which get the data from the database. The service class then creates a model with the question and corresponding answers that is returned to the controller which inturn returns to the client. <br/>
-- data/QuizData.py <br/> This is the layer that is responsible to make the database call and populate the Question and Answer models. <br/>
-- models/QuizModels.py <br/> This file represents the structure of the Question and Answer model with respect to the database tables. <br/>
-- static/templates <br/> This is where all the HTMl pages are stored. Routing from one page to another is integrated within the files. <br/>
-- static/css <br/> The required CSS for all the HTML pages <br/>
-- static/js <br/> This is where the client calls the launched server whenever Quizzes.html page is opened. This also contains all the processing for the experiment. <br/>
-- test.py –> Checks for broken links in the HTML pages and returns OK if none found. <br/>

## Bringing it together

-- Run run.py using the command "python3 run.py" <br/>
-- Make sure that the plugin earlier installed for the required browser is turned "ON" <br/>
-- Open Introduction.html in static/templates directory using a web browser and navigate as pleased. <br/>
-- Use test.py to check for broken links in the HTML pages. <br/>